const express = require('express');
const fs = require('fs');
var main = require('./js/main');
let app = express();
const router = express.Router();

app.use(express.static(__dirname));
router.get('/', (req, res) => {
    res.sendFile(__dirname + '/html/index.html');
});

router.get('/players', (req, res) => {
    res.sendFile(__dirname + '/html/players.html');
});

router.get('/slides', (req, res) => {
    res.sendFile(__dirname + '/html/slides.html');
});

app.use('/', router);

app.listen(3000, () => {
    console.log("server running")
})
